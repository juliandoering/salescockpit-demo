package net.nitro.salescockpit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class SalescockpitApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalescockpitApplication.class, args);
	}

}
