package net.nitro.salescockpit.domain.service;

import net.nitro.salescockpit.domain.model.quality.Quality;

import java.util.List;
import java.util.Optional;

public interface QualityRepository {

    List<Quality> findAll();

    Optional<Quality> findById(Long id);
}
