package net.nitro.salescockpit.domain.model.TradePartner;

import net.nitro.salescockpit.domain.model.Address;

import java.util.List;

public class TradePartner {

    private final TradePartnerType type;
    private final Account account;
    private final String name;
    private final String languageCode;
    private final Address billingAddress;
    private final List<Address> deliveryAddresses;

    public TradePartner(TradePartnerType type, Account account, String name, String languageCode, Address billingAddress, List<Address> deliveryAddresses) {
        this.type = type;
        this.account = account;
        this.name = name;
        this.languageCode = languageCode;
        this.billingAddress = billingAddress;
        this.deliveryAddresses = deliveryAddresses;
    }
}

