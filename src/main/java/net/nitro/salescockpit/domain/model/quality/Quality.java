package net.nitro.salescockpit.domain.model.quality;

import net.nitro.salescockpit.domain.model.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Entity.
 * Represents a Quality.
 */
public class Quality {

    private final QualityKey qualityKey;
    private final String name;
    private final String group;
    private final CoffeeCategory categroy;
    private final BigDecimal valuationDifferential;
    private final MonthYear valuationDate;
    private final PriceUnit priceUnit;
    private final Margin margin;
    private final List<Certification> certificationList;

    public Quality(QualityKey qualityKey, String name, String group, CoffeeCategory categroy, BigDecimal valuationDifferential, MonthYear valuationDate, PriceUnit priceUnit, Margin margin, List<Certification> certificationList) {
        this.qualityKey = qualityKey;
        this.name = name;
        this.group = group;
        this.categroy = categroy;
        this.valuationDifferential = valuationDifferential;
        this.valuationDate = valuationDate;
        this.priceUnit = priceUnit;
        this.margin = margin;
        this.certificationList = certificationList;
    }

    public QualityKey getQualityKey() {
        return qualityKey;
    }

    public String getName() {
        return name;
    }

    public String getGroup() {
        return group;
    }

    public CoffeeCategory getCategroy() {
        return categroy;
    }

    public BigDecimal getValuationDifferential() {
        return valuationDifferential;
    }

    public MonthYear getValuationDate() {
        return valuationDate;
    }

    public PriceUnit getPriceUnit() {
        return priceUnit;
    }

    public Margin getMargin() {
        return margin;
    }

    public List<Certification> getCertificationList() {
        return certificationList;
    }
}
