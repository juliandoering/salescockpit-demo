package net.nitro.salescockpit.domain.model;

import java.math.BigDecimal;

/**
 * Value Object.
 * Represents the market values.
 * Arabica price from stock market new york in cts/lb.
 * Robusta price from stock market london in usd/mt.
 * Exchange rate EUR-USD.
 * Exhcange rate PLN-USD.
 */
public class Market {

    private final MonetaryAmount arabicaPrice;
    private final MonetaryAmount robustaPrice;
    private final ExchangeRate eurUsdRate;
    private final ExchangeRate plnUsdRate;

    public Market(MonetaryAmount arabicaPrice, MonetaryAmount robustaPrice, BigDecimal eurUsdRate, BigDecimal plnUsdRate) {
        this.arabicaPrice = arabicaPrice;
        this.robustaPrice = robustaPrice;
        this.eurUsdRate = ExchangeRate.createEurUsdRate(eurUsdRate);
        this.plnUsdRate = ExchangeRate.createPlnUsdRate(plnUsdRate);
    }
}
