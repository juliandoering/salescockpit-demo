package net.nitro.salescockpit.domain.model.user;

import java.util.Objects;

/**
 * Value Object.
 * Represents a group company of the NKG.
 */
public class GroupCompany {

    private final String code;
    private final String name;

    public GroupCompany(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupCompany that = (GroupCompany) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name);
    }
}
