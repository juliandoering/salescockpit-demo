package net.nitro.salescockpit.domain.model;

import java.util.Objects;

/**
 * Value Object.
 * Represents an Origin.
 */
public class Origin {

    private final Long code;
    private final String name;
    private final String countryCode;

    public Origin(Long code, String name, String countryCode) {
        this.code = code;
        this.name = name;
        this.countryCode = countryCode;
    }

    public Long getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Origin origin = (Origin) o;
        return Objects.equals(code, origin.code) &&
                Objects.equals(name, origin.name) &&
                Objects.equals(countryCode, origin.countryCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, countryCode);
    }
}
