package net.nitro.salescockpit.domain.model.reservation;

import net.nitro.salescockpit.domain.model.Address;
import net.nitro.salescockpit.domain.model.TradePartner.TradePartner;
import net.nitro.salescockpit.domain.model.Trader;
import net.nitro.salescockpit.domain.model.user.GroupCompany;

import java.util.List;
import java.util.Objects;

/**
 * Entity.
 * A reservation aggregates reserved qualities.
 */
public class Reservation {

    private final List<ReservedQuality> reservedQualities;
    private final Trader trader;
    private final GroupCompany groupCompany;
    private final TradePartner tradePartner;
    private final Address deliveryAddress;
    private final String comment;

    public Reservation(List<ReservedQuality> reservedQualities, Trader trader, GroupCompany groupCompany, TradePartner tradePartner, Address deliveryAddress, String comment) {
        this.reservedQualities = reservedQualities;
        this.trader = trader;
        this.groupCompany = groupCompany;
        this.tradePartner = tradePartner;
        this.deliveryAddress = deliveryAddress;
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return Objects.equals(reservedQualities, that.reservedQualities) &&
                Objects.equals(trader, that.trader) &&
                Objects.equals(tradePartner, that.tradePartner) &&
                Objects.equals(deliveryAddress, that.deliveryAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reservedQualities, trader, tradePartner, deliveryAddress);
    }
}
