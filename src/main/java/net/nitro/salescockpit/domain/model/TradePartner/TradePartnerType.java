package net.nitro.salescockpit.domain.model.TradePartner;

public enum TradePartnerType {
    AGENT,
    BUYER,
    SELLER,
    SHIPPER;
}
