package net.nitro.salescockpit.domain.model;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Value Object.
 * Represents any exchange rate.
 * Provides factory methods for eur-usd and pln-usd.
 */
public class ExchangeRate {
    private final BigDecimal rate;
    private final ExchangeRateType type;

    private ExchangeRate(BigDecimal rate, ExchangeRateType types) {
        this.rate = rate;
        this.type = types;
    }

    public static ExchangeRate createEurUsdRate(BigDecimal rate) {
        return new ExchangeRate(rate, ExchangeRateType.EUR_USD);
    }

    public static ExchangeRate createPlnUsdRate(BigDecimal rate) {
        return new ExchangeRate(rate, ExchangeRateType.PLN_USD);
    }

    public BigDecimal getRate() {
        return rate;
    }

    public ExchangeRateType getTypes() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExchangeRate that = (ExchangeRate) o;
        return Objects.equals(rate, that.rate) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rate, type);
    }
}

