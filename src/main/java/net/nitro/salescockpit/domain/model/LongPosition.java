package net.nitro.salescockpit.domain.model;

import net.nitro.salescockpit.domain.model.quality.CoffeeCategory;
import net.nitro.salescockpit.domain.model.quality.Margin;
import net.nitro.salescockpit.domain.model.quality.Quality;
import net.nitro.salescockpit.domain.model.quality.QualityKey;

import java.math.BigDecimal;
import java.util.List;

/**
 * Entity.
 * Represents a LongPosition.
 */
public class LongPosition extends Quality {

    public LongPosition(QualityKey qualityKey, String name, String group, CoffeeCategory categroy, BigDecimal valuationDifferential, MonthYear valuationDate, PriceUnit priceUnit, Margin margin, List<Certification> certificationList) {
        super(qualityKey, name, group, categroy, valuationDifferential, valuationDate, priceUnit, margin, certificationList);
    }

    public BigDecimal calculateBaseFcaPrice() {
        // todo
        return BigDecimal.ZERO;
    }
}
