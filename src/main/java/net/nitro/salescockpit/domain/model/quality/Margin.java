package net.nitro.salescockpit.domain.model.quality;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Value Object.
 * Holds the margins for a Quality.
 */
public class Margin {

    private final BigDecimal a;
    private final BigDecimal b;
    private final BigDecimal c;
    private final BigDecimal individual;
    private final MarginType type;

    public Margin(BigDecimal a, BigDecimal b, BigDecimal c, BigDecimal individual, MarginType type) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.individual = individual;
        this.type = type;
    }

    public BigDecimal getA() {
        return a;
    }

    public BigDecimal getB() {
        return b;
    }

    public BigDecimal getC() {
        return c;
    }

    public BigDecimal getIndividual() {
        return individual;
    }

    public MarginType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Margin margin = (Margin) o;
        return Objects.equals(a, margin.a) &&
                Objects.equals(b, margin.b) &&
                Objects.equals(c, margin.c) &&
                Objects.equals(individual, margin.individual) &&
                Objects.equals(type, margin.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b, c, individual, type);
    }
}
