package net.nitro.salescockpit.domain.model;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Value Object.
 * Represents the currency to massunit relation.
 */
public class PriceUnit {

    private final String priceUnitName;
    private final String massUnitName;
    private final String currencyName;
    private final String symbol;
    private final BigDecimal currencyFactor;
    private final BigDecimal massUnitFactor;

    public PriceUnit(Long code, String priceUnitName, String massUnitName, String currencyName, String symbol, BigDecimal currencyFactor, BigDecimal massUnitFactor) {
        this.priceUnitName = priceUnitName;
        this.massUnitName = massUnitName;
        this.currencyName = currencyName;
        this.symbol = symbol;
        this.currencyFactor = currencyFactor;
        this.massUnitFactor = massUnitFactor;
    }

    public String getPriceUnitName() {
        return priceUnitName;
    }

    public String getMassUnitName() {
        return massUnitName;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public String getSymbol() {
        return symbol;
    }

    public BigDecimal getCurrencyFactor() {
        return currencyFactor;
    }

    public BigDecimal getMassUnitFactor() {
        return massUnitFactor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriceUnit priceUnit = (PriceUnit) o;
        return Objects.equals(priceUnitName, priceUnit.priceUnitName) &&
                Objects.equals(massUnitName, priceUnit.massUnitName) &&
                Objects.equals(currencyName, priceUnit.currencyName) &&
                Objects.equals(symbol, priceUnit.symbol) &&
                Objects.equals(currencyFactor, priceUnit.currencyFactor) &&
                Objects.equals(massUnitFactor, priceUnit.massUnitFactor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(priceUnitName, massUnitName, currencyName, symbol, currencyFactor, massUnitFactor);
    }
}
