package net.nitro.salescockpit.domain.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Value Object.
 * Monete value in relation to is price unit.
 * Store normalized values.
 */
public class MonetaryAmount {
    private final BigDecimal value;
    private final PriceUnit unit;

    public MonetaryAmount(BigDecimal value, PriceUnit unit) {
        this.value = value;
        this.unit = unit;
    }

    public BigDecimal normalized() {
        return value.divide(
                (unit.getCurrencyFactor().multiply(unit.getMassUnitFactor())),
                10, RoundingMode.HALF_UP
        );
    }

    public BigDecimal denormalized() {
        return value.multiply(unit.getMassUnitFactor()).multiply(unit.getCurrencyFactor());
    }
}
