package net.nitro.salescockpit.domain.model.reservation;

import net.nitro.salescockpit.domain.model.MonetaryAmount;
import net.nitro.salescockpit.domain.model.quality.MarginType;

import java.math.BigDecimal;

/**
 * Value Object.
 * Hols all price details that were relevant at the time of the reservation.
 */
public class PriceDetails {

    private final MonetaryAmount coffeeMonetaryAmount;
    private final BigDecimal exchangeRate;
    private final MonetaryAmount purchaseMonetaryAmount;
    private final MonetaryAmount costRate;
    private final MonetaryAmount valuationDifferential;
    private final MonetaryAmount adjustedValuationDifferential;
    private final MonetaryAmount monetaryAmountPerUnit;
    private final MonetaryAmount margin;
    private final MarginType marginType;

    public PriceDetails(MonetaryAmount coffeeMonetaryAmount, BigDecimal exchangeRate, MonetaryAmount purchaseMonetaryAmount, MonetaryAmount costRate, MonetaryAmount valuationDifferential, MonetaryAmount adjustedValuationDifferential, MonetaryAmount monetaryAmountPerUnit, MonetaryAmount margin, MarginType marginType) {
        this.coffeeMonetaryAmount = coffeeMonetaryAmount;
        this.exchangeRate = exchangeRate;
        this.purchaseMonetaryAmount = purchaseMonetaryAmount;
        this.costRate = costRate;
        this.valuationDifferential = valuationDifferential;
        this.adjustedValuationDifferential = adjustedValuationDifferential;
        this.monetaryAmountPerUnit = monetaryAmountPerUnit;
        this.margin = margin;
        this.marginType = marginType;
    }

    public MonetaryAmount getCoffeeMoneyValue() {
        return coffeeMonetaryAmount;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public MonetaryAmount getPurchaseMoneyValue() {
        return purchaseMonetaryAmount;
    }

    public MonetaryAmount getCostRate() {
        return costRate;
    }

    public MonetaryAmount getValuationDifferential() {
        return valuationDifferential;
    }

    public MonetaryAmount getAdjustedValuationDifferential() {
        return adjustedValuationDifferential;
    }

    public MonetaryAmount getMoneyValuePerUnit() {
        return monetaryAmountPerUnit;
    }

    public MonetaryAmount getMargin() {
        return margin;
    }

    public MarginType getMarginType() {
        return marginType;
    }
}

