package net.nitro.salescockpit.domain.model.user;

import java.util.List;
import java.util.Optional;

/**
 * Value Objects.
 * Holds the list of tenants that the user can access in Comtras.
 * Each of this entries can be the default group company.
 */
public class ComtrasGroupCompanies {

    private final List<GroupCompany> companies;

    public ComtrasGroupCompanies(List<GroupCompany> companies) {
        this.companies = companies;
    }

    public List<GroupCompany> getCompanies() {
        return companies;
    }

    public Optional<GroupCompany> findByCode(String code) {
        return this.companies.stream()
                .filter(gc -> gc.getCode().contentEquals(code))
                .findAny();
    }
}
