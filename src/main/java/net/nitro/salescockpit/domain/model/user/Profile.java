package net.nitro.salescockpit.domain.model.user;

import java.util.Objects;

public class Profile {

    private final String name;
    private final String email;
    private final String comtrasUsername;
    private final String comtrasPassword;
    private final GroupCompany groupCompany;
    private final ComtrasGroupCompanies comtrasGroupCompanies;

    public Profile(String name, String email, String comtrasUsername, String comtrasPassword, GroupCompany groupCompany, ComtrasGroupCompanies comtrasGroupCompanies) {
        this.name = name;
        this.email = email;
        this.comtrasUsername = comtrasUsername;
        this.comtrasPassword = comtrasPassword;
        this.groupCompany = groupCompany;
        this.comtrasGroupCompanies = comtrasGroupCompanies;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getComtrasUsername() {
        return comtrasUsername;
    }

    public String getComtrasPassword() {
        return comtrasPassword;
    }

    public GroupCompany getGroupCompany() {
        return groupCompany;
    }

    public ComtrasGroupCompanies getComtrasGroupCompanies() {
        return comtrasGroupCompanies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profile profile = (Profile) o;
        return Objects.equals(name, profile.name) &&
                Objects.equals(email, profile.email) &&
                Objects.equals(comtrasUsername, profile.comtrasUsername) &&
                Objects.equals(comtrasPassword, profile.comtrasPassword) &&
                Objects.equals(groupCompany, profile.groupCompany) &&
                Objects.equals(comtrasGroupCompanies, profile.comtrasGroupCompanies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email, comtrasUsername, comtrasPassword, groupCompany, comtrasGroupCompanies);
    }
}
