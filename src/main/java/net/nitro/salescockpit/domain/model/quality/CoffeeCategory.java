package net.nitro.salescockpit.domain.model.quality;

public enum CoffeeCategory {

    ARABICA,
    ROBUSTA;
}
