package net.nitro.salescockpit.domain.model.quality;

import java.util.Objects;

/**
 * Identity of a Quality.
 */
public class QualityKey {

    private final Long code;
    private final Integer cropYear;
    private final Long originCode;

    public QualityKey(Long code, Integer cropYear, Long originCode) {
        this.code = code;
        this.cropYear = cropYear;
        this.originCode = originCode;
    }

    public Long getCode() {
        return code;
    }

    public Integer getCropYear() {
        return cropYear;
    }

    public Long getOriginCode() {
        return originCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QualityKey that = (QualityKey) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(cropYear, that.cropYear) &&
                Objects.equals(originCode, that.originCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, cropYear, originCode);
    }
}
