package net.nitro.salescockpit.domain.model.reservation;

import net.nitro.salescockpit.domain.model.Market;
import net.nitro.salescockpit.domain.model.MonthYear;
import net.nitro.salescockpit.domain.model.PriceUnit;
import net.nitro.salescockpit.domain.model.quality.QualityKey;

import java.util.List;

/**
 * Entity.
 * A quality that belongs to a reservation.
 */
public class ReservedQuality {

    private final SalesType salesType;
    // reference to the quality
    private final QualityKey qualityKey;
    // can be null if sales type = short
    private final Long purchaseContractNumber;
    private final Integer bags;
    private final List<MonthYear> deliveryMonth;

    private final Market market;
    private final PriceUnit selectedPriceUnit;
    private final PriceDetails priceDetails;

    public ReservedQuality(SalesType salesType, QualityKey qualityKey, Long purchaseContractNumber, Integer bags, List<MonthYear> deliveryMonth, Market market, PriceUnit selectedPriceUnit, PriceDetails priceDetails) {
        this.salesType = salesType;
        this.qualityKey = qualityKey;
        this.purchaseContractNumber = purchaseContractNumber;
        this.bags = bags;
        this.deliveryMonth = deliveryMonth;
        this.market = market;
        this.selectedPriceUnit = selectedPriceUnit;
        this.priceDetails = priceDetails;
    }
}
