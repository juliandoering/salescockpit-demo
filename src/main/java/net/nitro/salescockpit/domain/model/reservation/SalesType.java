package net.nitro.salescockpit.domain.model.reservation;

public enum SalesType {

    SHORT,
    LONG;
}
