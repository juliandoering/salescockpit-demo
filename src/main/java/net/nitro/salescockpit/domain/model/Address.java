package net.nitro.salescockpit.domain.model;

import java.util.Objects;

/**
 * Value Object.
 * Represents an Address.
 */
public class Address {

    private final String name;
    private final String street;
    private final String additionalInformation;
    private final String zipCode;
    private final String city;
    private final String country;
    private final String countryCode;

    public Address(String name, String street, String additionalInformation, String zipCode, String city, String country, String countryCode) {
        this.name = name;
        this.street = street;
        this.additionalInformation = additionalInformation;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
        this.countryCode = countryCode;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(name, address.name) &&
                Objects.equals(street, address.street) &&
                Objects.equals(additionalInformation, address.additionalInformation) &&
                Objects.equals(zipCode, address.zipCode) &&
                Objects.equals(city, address.city) &&
                Objects.equals(country, address.country) &&
                Objects.equals(countryCode, address.countryCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, street, additionalInformation, zipCode, city, country, countryCode);
    }
}
