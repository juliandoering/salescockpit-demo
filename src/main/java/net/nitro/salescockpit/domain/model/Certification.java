package net.nitro.salescockpit.domain.model;

import java.util.Objects;

/**
 * Value Object.
 * Represents a certification that is put on a LongPosition.
 */
public class Certification {

    private final String name;
    private final String abbreviation;

    public Certification(String name, String abbreviation) {
        this.name = name;
        this.abbreviation = abbreviation;
    }

    public String getName() {
        return name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Certification that = (Certification) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(abbreviation, that.abbreviation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, abbreviation);
    }
}
