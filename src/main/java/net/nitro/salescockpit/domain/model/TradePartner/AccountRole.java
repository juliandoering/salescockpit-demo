package net.nitro.salescockpit.domain.model.TradePartner;

public enum AccountRole {
    DEBITOR,
    CREDITOR;
}
