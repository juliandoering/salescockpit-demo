package net.nitro.salescockpit.domain.model;

import java.util.Objects;

/**
 * Value Object.
 * Represents a date consisting of only a month and a year.
 */
public class MonthYear {

    private final int month;
    private final int year;

    public MonthYear(int month, int year) {
        this.month = month;
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MonthYear monthYear = (MonthYear) o;
        return month == monthYear.month &&
                year == monthYear.year;
    }

    @Override
    public int hashCode() {
        return Objects.hash(month, year);
    }
}
