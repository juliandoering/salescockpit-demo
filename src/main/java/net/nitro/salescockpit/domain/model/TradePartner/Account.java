package net.nitro.salescockpit.domain.model.TradePartner;

import java.util.Objects;

public class Account {
    private final Long number;
    private final AccountRole role;

    private Account(Long number, AccountRole role) {
        this.number = number;
        this.role = role;
    }

    public static Account createDebitor(Long accountNumber) {
        return new Account(accountNumber, AccountRole.DEBITOR);
    }

    public static Account createCreditor(Long accountNumber) {
        return new Account(accountNumber, AccountRole.CREDITOR);
    }

    public Long getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(number, account.number) &&
                role == account.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, role);
    }
}
