package net.nitro.salescockpit.domain.model;

/**
 * Different exchange rates.
 */
public enum ExchangeRateType {
    EUR_USD,
    PLN_USD;
}
