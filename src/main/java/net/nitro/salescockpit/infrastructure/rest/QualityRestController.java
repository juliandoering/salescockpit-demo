package net.nitro.salescockpit.infrastructure.rest;

import net.nitro.salescockpit.domain.model.quality.Quality;
import net.nitro.salescockpit.domain.service.QualityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class QualityRestController {

    private final QualityRepository repository;

    @Autowired
    public QualityRestController(QualityRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/qualities")
    List<Quality> all() {
        return repository.findAll();
    }

    @GetMapping("/qualities/{id}")
    Quality one(@PathVariable Long id) {
        // todo thorw not found exception
        return repository.findById(id).orElseThrow();
    }
}
