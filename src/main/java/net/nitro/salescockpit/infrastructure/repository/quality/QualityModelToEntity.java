package net.nitro.salescockpit.infrastructure.repository.quality;

import net.nitro.salescockpit.domain.model.quality.Quality;

import java.util.function.Function;

/**
 * Transforms the Quality model to the QualityEntity.
 *
 * todo     replace this function with factory methods
 */
public class QualityModelToEntity implements Function<Quality, QualityEntity>
{
    @Override
    public QualityEntity apply(Quality quality) {
        // todo
        return new QualityEntity();
    }
}
