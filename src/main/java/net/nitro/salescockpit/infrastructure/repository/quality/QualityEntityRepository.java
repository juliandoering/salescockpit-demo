package net.nitro.salescockpit.infrastructure.repository.quality;

import net.nitro.salescockpit.domain.model.quality.Quality;
import net.nitro.salescockpit.domain.service.QualityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Implements the mehtods provided by the QualityRepository interface.
 * Does the mapping between the Quality model and the QualityEntity.
 */
@Component
public class QualityEntityRepository implements QualityRepository {

    @Autowired
    private QualityJpaRepository repository;
    @Autowired
    private QualityModelToEntity toEntityMapper;
    @Autowired
    private QualityEntityToModel toModelMapper;


    /**
     * @return all Qualities.
     */
    @Override
    public List<Quality> findAll() {
        Iterable<QualityEntity> entities = repository.findAll();

        List<Quality> result = new ArrayList<>();
        Iterator<QualityEntity> iterator = entities.iterator();
        while (iterator.hasNext()) {
            result.add(toModelMapper.apply(iterator.next()));
        }

        return result;
    }

    @Override
    public Optional<Quality> findById(Long id) {
        Optional<QualityEntity> entity = repository.findById(id);
        if (entity.isPresent()) {
            Quality quality = toModelMapper.apply(entity.get());
            return Optional.of(quality);
        }
        return Optional.empty();
    }
}
