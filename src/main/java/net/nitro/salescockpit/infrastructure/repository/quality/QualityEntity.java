package net.nitro.salescockpit.infrastructure.repository.quality;

import net.nitro.salescockpit.domain.model.Certification;
import net.nitro.salescockpit.domain.model.quality.Margin;
import net.nitro.salescockpit.domain.model.PriceUnit;
import org.springframework.data.annotation.Id;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.math.BigDecimal;
import java.util.List;

/**
 * This is the database representation of a Quality.
 */
@Entity
public class QualityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // this three values combined represent a the QualityKey
    private Long comtrasCode;
    private Integer comtrasCrop;
    private Long comtrasOriginCode;

    private String name;
    private String group;
    private String categroy;
    private BigDecimal valuationDifferential;
    // todo think about inlining
    private Integer valuationMonth;
    private Integer valuationYear;
    // todo inline this
    private PriceUnit priceUnit;
    private Margin margin;
    private List<Certification> certificationList;

    // todo wird das gebraucht?
    private Long version;
}
