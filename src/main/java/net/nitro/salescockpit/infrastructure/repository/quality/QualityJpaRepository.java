package net.nitro.salescockpit.infrastructure.repository.quality;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QualityJpaRepository extends CrudRepository<QualityEntity, Long> {
}
